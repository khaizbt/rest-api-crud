<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
 use App\Siswa;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
    public function index(){
      return Siswa::all();
    }

    function create(Request $request){
      $siswa = new Siswa;
      $siswa->nama = $request->nama;
      $siswa->alamat = $request->alamat;


      $siswa->save();


      return "Data Berhasil Disimpan";

    }

    function update(Request $request,$id){

    $siswa = Siswa::find($id);
    $siswa->nama = $request->nama;
    $siswa->alamat = $request->alamat;
    $siswa->save();

    return "Data Berhasil diubah";
}
    function delete($id){
      $siswa = Siswa::find($id);
      $siswa->delete();

      return "Data Berhasil dihapus";
    }


}
